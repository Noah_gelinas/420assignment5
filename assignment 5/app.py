from flask import Flask, render_template, redirect, url_for, flash
from markupsafe import escape
from address import Address
from notes import Notes
from forms import AddressForm

app = Flask(__name__)

app.config['SECRET_KEY'] = 'a3717c186ecea5b15c14fee1907f9477'

addresses=[Address("Noah","123 Fake Street","Montreal","QC"), Address("Tommy","69 420 Street","Great wall city","China"),Address("Kenny","1112 Verdun Street","Montreal","QC")]
notes=[Notes(1,"I am owner of id 1"),
       Notes(2,"I am the owner of id 2"),
       Notes(3,"I am the owner of id 3")]


@app.route("/")
@app.route("/home")
def home_view():
    '''home page'''
    context_data= {
        'page_title':'add address'
    }
    return render_template('home.html',context=context_data)

@app.route("/addressbook/<name>")
def addressbook_view(name):
    '''list address based on name'''
    name=escape(name)
    for address in addresses:
        if address.name == name:
            context_data={'page_title':'Specific Address','Name':f'{name}','Street':address.street,'City':address.city, 'Province':address.province}
            return render_template("specific_address.html",context = context_data)
    flash("That address does not exist.","error")
    return redirect(url_for("addressbooklist_view"))

@app.route('/addressbook',methods=['GET','POST'])
def addressbooklist_view():
    '''list all addresses'''
    form=AddressForm()
    context_data= {
        'page_title':'add address'
    }
    if form.validate_on_submit():
        exists=False
        for address in addresses:
            if address.city==form.city.data and address.province==form.province.data and address.street==form.street.data:
                exists=True
                flash("Address Already Exists",'error')

        if exists is False:
            flash('User created successfully.','success')
            addresses.append(Address(form.name.data,form.street.data,form.city.data,form.province.data))
        return redirect(url_for('addressbooklist_view'))
    return render_template("addresses.html",addresses=addresses,context=context_data,form=form)

@app.route("/notes/<id>")
def notes_view(id):
    '''list note based on id'''
    id=escape(id)

    for note in notes:
        if str(note.id) == id:
            return f'<h1>{str(note)}</h1>'
    return redirect(url_for("noteslist_view"))

@app.route("/notes")
def noteslist_view():
    '''list all notes'''
    context_data= {
        'page_title':'add address'
    }
    return render_template("listnotes.html",notes=notes,context=context_data)

@app.errorhandler(404)
def page_not_found(e):
    '''handles page not found error'''
    return render_template('custom404.html'),404

if __name__ == '__main__':
    app.run(port=5002, debug=True)
