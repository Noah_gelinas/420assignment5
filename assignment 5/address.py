# from database_ import Database

class Address:
    '''class creating an address'''
    def __init__(self,name,street,city,province):
        self.name=name
        self.street=street
        self.city=city
        self.province=province
    def __str__(self):
        return f"{self.name}, {self.city}, {self.province}"
