from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class AddressForm(FlaskForm):
    '''form to create new address'''
    name=StringField('Name',validators=[DataRequired()])
    street=StringField('Street name',validators=[DataRequired()])
    city=StringField('City name',validators=[DataRequired()])
    province=StringField('Province name',validators=[DataRequired()])
    submit=SubmitField("Submit new address")
