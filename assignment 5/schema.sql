DROP TABLE NOTE;
DROP TABLE ADDRESS_BOOK;


CREATE TABLE ADDRESS_BOOK (
author_id NUMBER(4) PRIMARY KEY,
user_name VARCHAR2(25),
street VARCHAR2(50),
city VARCHAR2(50),
province VARCHAR2(50)
);

CREATE TABLE NOTE (
note_id NUMBER(4) PRIMARY KEY,
author_id NUMBER(4),
content VARCHAR2(100),
constraint author_fk FOREIGN KEY (author_id)
REFERENCES ADDRESS_BOOK(author_id)
);

INSERT INTO ADDRESS_BOOK (author_id,user_name,street,city,province)
VALUES(1001, 'Sara', 'street 1', 'LAVAL', 'QC');
INSERT INTO ADDRESS_BOOK (author_id,user_name,street,city,province)
VALUES(1002, 'Mays', 'street 2', 'Toronto', 'ON');
INSERT INTO ADDRESS_BOOK (author_id,user_name,street,city,province)
VALUES(1003, 'Jamil', 'street 3', 'MONTREAL', 'QC');

INSERT INTO NOTE(note_id,author_id,content)
VALUES(1001,1001,'Do you know who am I?');
INSERT INTO NOTE(note_id,author_id,content)
VALUES(1002,1001,'I go to college');
INSERT INTO NOTE(note_id,author_id,content)
VALUES(1003,1002,'I was very young');
INSERT INTO NOTE(note_id,author_id,content)
VALUES(1004,1003,'Life is too too short');
INSERT INTO NOTE(note_id,author_id,content)
VALUES(1005,1003,'Need all kinds of strengths to fully succeed!');