import os
import oracledb
from config_db import usr,pw,host,sn


class Database:
    def __init__(self, autocommit=True):
        self.__connection = self.__connect()
        self.__connection.autocommit = autocommit

    def __connect(self):
        return oracledb.connect(user=usr, password=pw,host=host,  service_name=sn)
    
    def  db_conn (self):
        return self.__connection

    def __run_file(self, file_path):
        statement_parts = []
        with self.__connection.cursor() as cursor:
            # pdb.set_trace()
            with open(file_path, 'r') as f:
                for line in f:
                    if line[:2]=='--': continue
                    statement_parts.append(line)
                    if line.strip('\n').strip('\n\r').strip().endswith(';'):
                        statement = "".join( statement_parts).strip().rstrip(';')
                        if statement:
                            try:
                                # pdb.set_trace()
                                cursor.execute(statement)
                            except Exception as e:
                                print(e)
                        statement_parts = []

    
    def add_address(self, address):
        '''Add an address to the DB for the given Address object'''
        pass

    def get_address(self, name):
        '''Returns an Address object based on the provided name'''
        pass

    # def get_addresses(self):
    #     '''Returns all Address objects in a list'''
    #     pass

    def close(self):
        '''Closes the connection'''
        if self.__connection is not None:
            self.__connection.close()
            self.__connection = None

    def get_cursor(self):
            for i in range(3):
                try:
                    return self.__connection.cursor()
                except Exception as e:
                    # Might need to reconnect
                    self.__reconnect()

    def __reconnect(self):
        try:
            self.close()
        except oracledb.Error as f:
            pass
        self.__connection = self.__connect()

    

    def run_sql_script(self, sql_filename):
        '''run sql script'''
        if os.path.exists(sql_filename):
            self.__connect()
            self.__run_file(sql_filename)
            self.close()
        else:
            print('Invalid Path')
# ----------------------------------
# ------- DML DB Operations  
    def get_addresses(self, cond):
        ''' method to list all (if cond is True) or some address books based on cond'''
        if cond is True:
            with self.__connection.cursor() as cur:
                qry ="select * from ADDRESS_BOOK"
                try:
                    r = cur.execute(qry).fetchall()
                    return r
                except Exception as e:
                    print(e)
        else:
            pass
# # ----------------------
    def add_new_address(self, author_id ,user_name, street, city, province):
        '''  method to add a new address, data coming fro a user input form'''
        
        pass
# ----------------------
    def get_address_field(self, field, cond):
        '''  method to get the column field based on condition'''
        pass
   
# ----------------------                   
    def get_notes_user(self, author_id):
        ''' method to list all notes of a given user '''
        with self.__connection.cursor() as cur:
            qry=f'select * from note where author_id = {author_id}'
            try:
                r=cur.execute(qry).fetchall()[0]
                return r
            except Exception as e:
                print(e)
        pass
# ----------------------------
# ----------------------------
#  1.	Create two global variables:
# •	an instance of Database class that you call db.
db = Database()
# 2.	Call run_sql_script on database.sql  if the script (database.py) is run in isolation.
db.run_sql_script('schema.sql')

if __name__ == '__main__':
    pass